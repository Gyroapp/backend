class AddVegetarianToItems < ActiveRecord::Migration[5.1]
  def change
    add_column :items, :vegetarian?, :boolean 
  end
end
