class OrdersController < ApplicationController
    def index_by_user_id
        orders = Order.where(user_id: order_params[:user_id])
        render json: orders.to_json(include: :item)
    end
    
    def index_by_item_id
        orders = Order.where(item_id: order_params[:item_id])
        render json: orders.to_json(include: :item)
    end
    
    private
    
    def order_params
        params.permit(:user_id, :item_id)
    end
end
